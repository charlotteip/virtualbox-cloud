virtualbox-cloud
================

juju charms to create a poor man's cloud with Ubuntu+VirtualBox

Ingredients
-----------

- Ubuntu MAAS ("Metal As A Service") provisions bare-metal compute. http://maas.ubuntu.com.
- Ubuntu JuJu orchestrates cloud applications created from services ("charms".) http://juju.ubuntu.com.
- VirtualBox is dead-simple virtualization that runs everywhere. htts://www.virtualbox.org.
- phpVirtualBox is a web management console for VirtualBox VMs. http://sourceforge.net/projects/phpvirtualbox.
- Apache is added as a binding agent. As always.

Recipe (WIP)
------------

As with any cloud computing recipe, set aside plenty of grok time. Also grog time.

0. First, install MAAS and set up bare-metal provisioning.

  0. Select a fresh server. This will be the Controller. Make sure it is set ot power on whenever power appears.
  0. Add MAAS to the Controller, taking care to form it into a DHCP server. Clear other DHCP servers from the area.
  0. Select one or more additional servers having IPMI or other remote-control capabilities. These will be your Nodes.
  0. Add PXE booting to your Node BIOS configurations and make sure they are set to power on whenever power appears.
  0. Connect the Nodes to the same network as the Controller and power them on.
  0. Enroll the Nodes into your MAAS cluster using the MAAS web console.
 
0. Great, you have a pile of metal to provision. Edison would be proud! Now, let's install JuJu and do something with it.

  0. Install JuJu to the Controller.
  0. Bootstrap JuJu for MAAS.
  0. Try deploying some existing JuJu charms to the Nodes.

0. So far, so good. We now have a working cluster of Ubuntu application Nodes. Let's virtualize them.

  0. Export one of your VMs to an ovp file. Version 2.0 is preferred. Keep the networking interface simple (NAT!)
  0. Put the ovp file on a web server. The Controller Apache DocumentRoot is a good spot.
  0. Grab the VirtualBox Charm and launch it with the URI to your OVP file.
  0. After the charm starts, you should have remote access (using RDP) to your running virtual machine.

0. OK, we launched one virtual machine. Now let's herd a bunch of them around.

  0. Get the phpVirtualBox charm and fire it up.
  0. Connect it to your already running VirtualBox charm.
  0. You should now be able to manage your VirtualBox Node.
  0. Run additional VirtualBox charms and connect them to the phpVirtualBox charm.

Congrats, you now have a Poor Man's Cloud. Let Total World Domination ensue!

Status
------

1. WIP: Building the VBox charm.
2. TBD: Build the phpVbox charm.
3. TBD: Build / pick a storage charm for snapshots if needed. S3, etc?
