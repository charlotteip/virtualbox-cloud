JuJu
----

### Charm Debug

after [deploying a charm or changing it's config options](#charm-deployment)...

1. Watch the debug logger to see what happens `juju debug-log`
2. If needed, launch a shell into the failed unit to diagnose `juju ssh virtualbox4/0`
3. Edit the charm to fix any issues.
4. Forcibly upgrade the charm. THis will replace the charm files on the failed unit irregardless of error conditions. 

    ```sh
    juju upgrade-charm --repository=/home/sysadmin/dev/virtualbox-cloud/charms/ virtualbox4 --force
    ```
5. Tell JuJu to resolve the charm error and retry last action `juju resolved virtualbox4/0 --retry`
 
(Steps 4 and 5 are dependent on your charm being able to handle any leftover error state. It'sprobably a good idea to make this work as it sort of indicates that your charm can recover from tangled error state.)

<a name="charm-deployment"/>
### Charm Deployment and Configuration

1. Deploy a charm from filesystem on JuJu environment box

    ```sh
    juju deploy --repository=/home/sysadmin/dev/virtualbox-cloud/charms/ local:trusty/virtualbox4 <service name>
    ```
    
    > Note: All units with the same service name will share the same configuration, and react identically to configuration changes, which is probably not what you want for VirtualBox deployments. You probalby want to create a new service name one-to-one for each VM you will be launching to maintain independent control. A possible exception is if you simply want to create a large number of blank VBox units and manage them externally with phpVirtualBox or something like it.
    
2. get environment status `juju status`
3. set a charm config setting, this one launches a VM with a Nano Linux ISO 
    
    ```sh 
    juju set virtualbox4 boot-iso-uri="http://sourceforge.net/projects/nanolinux/files/nanolinux-1.2.iso/download"`
    ```
4. unset a charm config setting back to its default `juju unset virtualbox4 boot-iso-uri
5. Rarely, start another unit for the charm `juju add-unit virtualbox4`. The new unit will assume the current configuation for the service and will track it in lockstep with other service units. 
    
    > Warning: See #1 for why this might be a bad thing.

### Restarting A Hosed JuJu Environment

tbd

### General Issues and Solutions

1. Charm deployment is stuck in Pending state. 
   1. MAAS box is probably stuck. Reset it with IPMI or whtever you use. The default state for provisioned but inactive MAAS boxes is powered off.
2. Controller gets really slow
  1. beam.smp, a RabbitMQ-related process was consuming lots of CPU. Had to reboot controller. Happened every 2-3 days while developing.


### Sorting Bin

```sh
juju --force remove-unit virtualbox/0
juju bootstrap

juju charm create virtualbox
juju deploy --to 0 juju-gui
juju deploy juju-gui

juju destroy-unit virtualbox/0

juju expose juju-gui

juju remove-charm local:trusty/virtualbox-1
juju remove-machine 1
juju remove-service juju-gui
juju remove-service virtualbox4
juju remove-unit virtualbox4/0
```

